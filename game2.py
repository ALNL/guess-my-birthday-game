from random import randint
name = input("Hi! What is your name? ")
for i in range(5):
    month = randint(1,12)
    year = randint(1924,2004)
    guess = input(f"{name}, were you born in {month}/{year}? yes or no?").lower()
    if guess == "yes":
        print("I knew it!")
        break
    else:
        print("Drat! Lemme try again!")
    if i == 4:
        print("I have other things to do. Good bye.")
        
